﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;

namespace M3UF3P1_RamosPascualVictor
{
    class WordToMayus
    {
        public void WordMayus()
        {
            Console.Write("Introduzca una palabra para convertir en mayúsculas (escriba END para salir): ");
            string palabra = Console.ReadLine();

            while (palabra != "END")
            {
                string originalpath = @"..\..\..\..\transfText.txt";
                string textoOriginal = File.ReadAllText(originalpath);

                string palabraMayus = palabra.ToUpper();

                // Reemplazar las ocurrencias de la palabra original por la palabra en mayúsculas
                string textoModificado = textoOriginal.Replace(palabra, palabraMayus);


                string newpath = @"..\..\..\..\transfText.txt";

                File.WriteAllText(newpath, textoModificado);

                Console.WriteLine("El archivo transfText.txt ha sido creado con éxito.");

                Console.Write("\nIntroduzca otra palabra para convertir en mayúsculas (escriba END para salir): ");
                palabra = Console.ReadLine();
            }
        }
    }
}
