﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;

namespace M3UF3P1_RamosPascualVictor
{
    class ChooseLang
    {
        public void Lenguajes()
        {
            string path = @"..\..\..\..\Files\allLanguages.txt";
            if (File.Exists(path))
            {
                Leertexto(path);
                SeleccionarIdioma();
            }
            else
            {
                Console.WriteLine("No existe el fichero");
            }
            
            
            

        }

        void Leertexto(string path)
        {

            string text = File.ReadAllText(path);
            Console.WriteLine(text);
            Console.WriteLine("Presione cualquier tecla para continuar.");
            Console.ReadKey();
        }

        void SeleccionarIdioma()
        {
            string pathEsp = @"..\..\..\..\Files\lang\es.txt";
            string pathCat = @"..\..\..\..\Files\lang\cat.txt";
            string pathEn = @"..\..\..\..\Files\lang\en.txt";

            string idioma = Respuestas("Que idioma vols [cat / es / en] ?").ToLower();

            if (idioma == "cat")
            {
                Leertexto(pathCat);
            }
            else if (idioma == "es")
            {
                Leertexto(pathEsp);
            }
            else if (idioma == "en")
            {
                Leertexto(pathEn);
            }
            else
            {
                Leertexto(pathCat);
            }
        }

        public string Respuestas(string message)
        {
            Console.WriteLine(message);
            return Console.ReadLine();
        }
    }
}
