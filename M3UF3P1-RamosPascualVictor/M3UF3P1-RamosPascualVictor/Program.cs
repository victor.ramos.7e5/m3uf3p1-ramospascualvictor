﻿using System;
using System.Collections.Generic;
using System.IO;

namespace M3UF3P1_RamosPascualVictor
{
    class Program
    {
        static void Main(string[] args)
        {
            Program p = new Program();
            ChooseLang ch = new ChooseLang();
            SavingLoadGame sl = new SavingLoadGame();
            WordToMayus wm = new WordToMayus();
            //p.NotesFriendly();
            //ch.Lenguajes();
            sl.Ejercicio();
            //wm.WordMayus();
        }

        void NotesFriendly()
        {

            string path = @"..\..\..\..\notestotales.txt";
            StreamReader input = new StreamReader(path);

            int countExcelent = 0;
            int countNotable = 0;
            int countAprovat = 0;
            int countSuspes = 0;

            
            string line;
            while ((line = input.ReadLine()) != null)
            {
               
                double nota = double.Parse(line);

                
                if (nota >= 9)
                {
                    countExcelent++;
                }
                else if (nota >= 6.5)
                {
                    countNotable++;
                }
                else if (nota >= 5)
                {
                    countAprovat++;
                }
                else
                {
                    countSuspes++;
                }
            }

            input.Close();

            string newpath = @"..\..\..\..\notesHistory.txt";
            StreamWriter output = new StreamWriter(newpath);

            output.WriteLine("Excel·lent: {0}", countExcelent);
            output.WriteLine("Notable: {0}", countNotable);
            output.WriteLine("Aprovat: {0}", countAprovat);
            output.WriteLine("Suspès: {0}", countSuspes);

            
            output.Close();

        }

    }
}
