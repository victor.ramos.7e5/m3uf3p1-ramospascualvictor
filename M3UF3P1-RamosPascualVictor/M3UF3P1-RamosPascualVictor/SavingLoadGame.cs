﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;

namespace M3UF3P1_RamosPascualVictor
{
    class SavingLoadGame
    {
        static string playerName;
        static int playerLevel;
        static string playerRole;
        public void Ejercicio()
        {
            Console.WriteLine("Introduce el nombre de jugador");
            playerName = Console.ReadLine();
            Menu();
        }
        void MostrarMenu()
        {
            Console.WriteLine("[1]: Crear Partida");
            Console.WriteLine("[2]: Guardar Partida");
            Console.WriteLine("[3]: Cargar Partida Guardada");
            Console.WriteLine("[4]: Modificar Partida");
            Console.WriteLine("[5]: Eliminar Partida");
            Console.WriteLine("[6]: Salir");
        }
        void Menu()
        {
            
            bool salir = false;
            while (!salir)
            {
                Console.Clear();
                MostrarMenu();
                int opcion = int.Parse(Console.ReadLine());
                switch (opcion)
                {
                    case 1:
                        CrearNuevaPartida();
                        break;
                    case 2:
                        GuardarPartida();
                        break;
                    case 3:
                        CargarPartidaGuardada();
                        break;
                    case 4:
                        ModificarPartida();
                        break;
                    case 5:
                        Eliminarpartida();
                        break;
                    case 6:
                        salir = true;
                        Console.WriteLine("Adios!");
                        break;
                }
            }
        }
        void CrearNuevaPartida()
        {
            Console.WriteLine("Creando nueva partida...");

            Console.WriteLine("Tu nombre de jugador es ", playerName);
            playerLevel = 0;
            Console.WriteLine("Elige un rol de los siguientes");

            string text = File.ReadAllText(@"..\..\..\..\Game\game.config.txt");
          
            string[] roles = text.Split(':', ',');
            for (int i = 0; i < roles.Length; i++)
            {
                Console.WriteLine(i + 1 + ". " + roles[i]);
            }

            Console.Write("Seleccione su rol por numero: ");
            int seleccion = int.Parse(Console.ReadLine());
            playerRole = roles[seleccion - 1];

            Console.WriteLine("Partida creada exitosamente!");
        }
        void GuardarPartida()
        {
            if (playerName == null)
            {
                Console.WriteLine("Crea una nueva partida antes de guardar una.");
                return;
            }

            string filePath = @"..\..\..\..\Game\" + playerName + "_savedGame.txt";

            using (StreamWriter writer = new StreamWriter(filePath))
            {
                writer.WriteLine(playerName);
                writer.WriteLine(playerLevel);
                writer.WriteLine(playerRole);
            }

            Console.WriteLine("Partida guardada exitosamente!");
            Console.WriteLine("Pulsa una tecla para continuar");
            Console.ReadLine();
        }
        void CargarPartidaGuardada()
        {
            Console.WriteLine("Cargando partida guardada...");

            string filePath = @"..\..\..\..\Game\" + playerName + "_savedGame.txt";
            if (!File.Exists(filePath))
            {
                Console.WriteLine("No se ha encontrado una partida guardada para " + playerName);
                return;
            }

            using (StreamReader reader = new StreamReader(filePath))
            {
                playerName = reader.ReadLine();
                playerLevel = int.Parse(reader.ReadLine());
                playerRole = reader.ReadLine();
            }

            Console.WriteLine("Partida cargada exitosamente!");
            Console.WriteLine("Pulsa una tecla para continuar");
            Console.ReadLine();
        }
        void ModificarPartida()
        {
            if (playerName == null)
            {
                Console.WriteLine("No hay partida cargada.");
                return;
            }
            else
            {
                Console.WriteLine("Introduce el nuevo nivel del jugador");

                int nuevoNivel = int.Parse(Console.ReadLine());

                playerLevel = nuevoNivel;
            }
            Console.WriteLine("Pulsa una tecla para continuar");
            Console.ReadLine();
        }

        void Eliminarpartida()
        {
            Console.WriteLine("Eliminando partida guardada");

            string filePath = @"..\..\..\..\Game\" + playerName + "_savedGame.txt";
            if (!File.Exists(filePath))
            {
                Console.WriteLine("No se ha encontrado una partida guardada para " + playerName);
                return;
            }
            else
            {
                File.Delete(filePath);
            }
            Console.WriteLine("Pulsa una tecla para continuar");
            Console.ReadLine();
        }

    }
}
